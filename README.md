# Weather App
A little angularjs app to try out a great api by OpenWeatherMaps. 

## Demo 
Hosted at: https://weather-app-demo.azurewebsites.net/

## Local Installation 

Download and install 
- [NodeJS](https://nodejs.org/en/)
- [Git](https://git-scm.com/)

Clone this repo 
```
> git clone https://bPratik@bitbucket.org/bPratik/weather-app-demo.git
> cd weather-app-demo
``` 

Install these packages 
```
> npm install -g karma-cli
> npm install -g bower
> npm install
> bower install
```

Launch for local debugging
```
> npm start
```
This starts a server listening on `localhost:9090`. It can be changed in `package.json`. 

## Testing 
Jasming unit tests run using Karma. 
```
> npm test
# OR
> karma start
```

