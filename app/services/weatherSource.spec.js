describe("DataService", function () {
    beforeEach(angular.mock.module("WeatherApp"));
    it("can be instantiated", inject(function (_weatherSource_) {
        expect(_weatherSource_).toBeDefined();
    }));
});

describe("DataService", function () {
    var $httpBackend;
    var weatherSource;
    beforeEach(angular.mock.module("WeatherApp"));
    beforeEach(angular.mock.module("WeatherApp", function ($provide) {
        $provide.constant("OWM_API_URL", "/weatherapi/forecast");
    }));
    beforeEach(inject(function (_$httpBackend_, _weatherSource_) {
        $httpBackend = _$httpBackend_;
        weatherSource = _weatherSource_;
    }));
    afterEach(function () {
        // Verify that there are no outstanding expectations or requests after each test
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
    
    it("updates the location property on calling `refresh()`", function () {
        var rawData = {
            "city": {
                "name": "City of Edinburgh",
                "country": "GB"
            },
            "list": []
        };
        $httpBackend.expectGET("/weatherapi/forecast").respond(rawData);

        expect(weatherSource.location).toEqual("");

        weatherSource.refresh();
        $httpBackend.flush();

        expect(weatherSource.location).toEqual("City of Edinburgh, GB");
    });

    it("parses one day correctly from `list` on calling `refresh()`", function () {
        var singleDay = {
            "city": {
                "name": "City of Edinburgh",
                "country": "GB"
            },
            "list": [                {
                "dt": 1520089200,
                "dt_txt": "2018-03-03 15:00:00"
            },
            {
                "dt": 1520100000,
                "dt_txt": "2018-03-03 18:00:00"
            }]
        };

        $httpBackend.whenGET("/weatherapi/forecast").respond(singleDay);
        weatherSource.refresh();
        $httpBackend.flush();

        expect(Object.keys(weatherSource.days).length).toEqual(1);
    });

    it("parses two days correctly from `list` on calling `refresh()`", function () {
        var twoDays = {
            "city": {
                "name": "City of Edinburgh",
                "country": "GB"
            },
            "list": [                {
                "dt": 1520089200,
                "dt_txt": "2018-03-03 15:00:00"
            },
            {
                "dt": 1520100000,
                "dt_txt": "2018-03-03 18:00:00"
            },
            {
                "dt": 1520380800,
                "dt_txt": "2018-03-07 00:00:00"
            }]
        };

        $httpBackend.whenGET("/weatherapi/forecast").respond(twoDays);
        weatherSource.refresh();
        $httpBackend.flush();

        expect(Object.keys(weatherSource.days).length).toEqual(2);
    });
});

