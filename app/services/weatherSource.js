(function () {
    var app = angular.module("WeatherApp");

    function WeatherSourceService($http, OWM_API_URL) {
        var service = {
            refresh: refresh,
            days: {},
            location: "",
            isLoading: false
        };
        var rawData = null;

        function refresh() {
            service.isLoading = true;
            return $http.get(OWM_API_URL)
                .then(_persist)
                .then(_transform);
        }

        function _persist(response) {
            service.isLoading = false;
            rawData = response.data;
            return response;
        }

        function getTitleFromDate(date) {
            var dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            return dayNames[date.getDay()] + ", " + date.getDate() + " " + monthNames[date.getMonth()];
        }

        function _transform(response) {
            service.location = response.data.city.name + ", " + response.data.city.country;
            var count = rawData.list.length;
            var days = {};
            for (var i = 0; i < count; i++) {
                var forecast = rawData.list[i];
                var date = new Date(forecast.dt * 1000);
                var dayId = "Day-" + date.getDate();
                if(!days[dayId]){
                    days[dayId] = { 
                        title: getTitleFromDate(date),
                        hours: []
                    };
                }
                days[dayId].hours.push(forecast);
            }
            service.days = days;
            return response;
        }

        return service;
    }
    WeatherSourceService.$inject = ["$http", "OWM_API_URL"];
    app.service("weatherSource", WeatherSourceService);
})();

