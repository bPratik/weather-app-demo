(function () {
    var app = angular.module("WeatherApp");
    // app.constant("OWM_API_URL", "http://samples.openweathermap.org/data/2.5/forecast?id=524901&appid=b6907d289e10d714a6e88b30761fae22");
    app.constant("OWM_API_URL", "https://api.openweathermap.org/data/2.5/forecast?id=3333229&units=metric&appid=530bae972c642a18e57d10ddba7a3a63");
})();
