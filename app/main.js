(function () {
    var app = angular.module("WeatherApp");

    function MainController($scope, weatherSource) {
        var $ctrl = this;
        var unwatch;
        $ctrl.ready = false;
        $ctrl.$postLink = function () {
            unwatch = $scope.$watch(
                function () {
                    return weatherSource.days;
                },
                function (newValue) {
                    if(newValue && Object.keys(newValue).length){
                        unwatch();
                        $ctrl.ready = true;
                    }
                }, true);
        };
    }
    MainController.$inject = ["$scope", "weatherSource"];

    app.controller("mainController", MainController);
})();
