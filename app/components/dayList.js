(function () {

    function DayListController(weatherSource) {
        var $ctrl = this;
        // $ctrl.days = [];
        $ctrl.hours = [];
        $ctrl.selectedIndex = null;

        $ctrl.setHours = function ($index, day) {
            $ctrl.selectedIndex = $index;
            $ctrl.hours = day.hours;
        };

        $ctrl.$onInit = function () {
            $ctrl.weatherSource = weatherSource;
            // $ctrl.days = weatherSource.days;
        };
    }
    DayListController.$inject = ["weatherSource"];

    angular.module("WeatherApp").component("dayList", {
        templateUrl: "components/dayList.html",
        controller: DayListController
    });

})();