(function () {
    function WelcomeBannerController(weatherSource) {
        var $ctrl = this;
        $ctrl.remote = weatherSource;

        $ctrl.load = function () {
            $ctrl.remote.refresh();
        };
    }
    WelcomeBannerController.$inject = ["weatherSource"];

    angular.module("WeatherApp").component("welcomeBanner", {
        templateUrl: "components/welcomeBanner.html",
        controller: WelcomeBannerController
    });
})();